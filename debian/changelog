django-jinja (2.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version.
  * Update copyright year.

 -- Edward Betts <edward@4angle.com>  Thu, 07 Sep 2023 20:48:00 +0100

django-jinja (2.10.2-2) unstable; urgency=medium

  * Update debian/watch to check GitHub tags instead of releases.
  * Update Standards-Version.

 -- Edward Betts <edward@4angle.com>  Sun, 11 Dec 2022 06:03:30 +0000

django-jinja (2.10.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + python3-django-jinja: Drop versioned constraint on python3-django in
      Depends.

  [ Edward Betts ]
  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Fri, 27 May 2022 11:22:56 +0200

django-jinja (2.10.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright, upstream now includes a copy of normalize.css.
  * Update copyright year.

 -- Edward Betts <edward@4angle.com>  Sun, 02 Jan 2022 11:15:14 +0000

django-jinja (2.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version.

 -- Edward Betts <edward@4angle.com>  Sat, 13 Nov 2021 07:39:11 +0000

django-jinja (2.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch for upstream filename changes.

 -- Edward Betts <edward@4angle.com>  Wed, 25 Aug 2021 16:25:55 +0100

django-jinja (2.7.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Edward Betts ]
  * New upstream release.
  * Switch from dh --with python in debian/rules to a dh-sequence-python3
    Build-Depends.
  * Remove django_jinja.egg-info during clean target.
  * Update python3-django Depends version, upstream removed support for older
    versions.

 -- Edward Betts <edward@4angle.com>  Sun, 01 Nov 2020 09:16:19 +0000

django-jinja (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version.
  * Set Rules-Requires-Root to no.
  * Update copyright year.
  * Update package description.
  * Use github as upstream source instead of pypi.
  * Install upstream changelog.

 -- Edward Betts <edward@4angle.com>  Mon, 10 Feb 2020 14:38:15 +0000

django-jinja (2.4.1-2) unstable; urgency=medium

  [Emmanuel Arias]
  * Team upload
  * Remove Python 2 support.
  * Bump Standards-Versions to 4.4.0
  * Bump debhelper-compat to 12

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Aug 2019 23:12:48 +0200

django-jinja (2.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).
  * Update debian/compat to 11.
  * Use https in debian/watch.
  * Remove trailing space from debian/changelog entry.
  * Update years in debian/copyright.

 -- Edward Betts <edward@4angle.com>  Tue, 16 Jan 2018 10:27:06 +0000

django-jinja (2.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version (no changes).

 -- Edward Betts <edward@4angle.com>  Thu, 05 Oct 2017 09:42:09 +0100

django-jinja (2.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Sun, 30 Apr 2017 21:23:06 +0100

django-jinja (2.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Tue, 03 Jan 2017 14:19:05 +0000

django-jinja (2.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 31 Aug 2016 11:53:06 +0100

django-jinja (2.1.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control: update Standards-Version to 3.9.8

 -- Edward Betts <edward@4angle.com>  Fri, 27 May 2016 09:40:31 +0100

django-jinja (2.1.2-2) unstable; urgency=low

  * debian/rules: fix invalid Vcs-Git URL

 -- Edward Betts <edward@4angle.com>  Tue, 23 Feb 2016 07:34:46 +0000

django-jinja (2.1.2-1) unstable; urgency=low

  * New upstream release.
  * debian/control: no need for version on python-all Build-Depends
  * debian/control: Vcs-Git: switch to https
  * debian/control: update Standards-Version (no changes)

 -- Edward Betts <edward@4angle.com>  Sun, 21 Feb 2016 08:51:46 +0000

django-jinja (2.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 23 Dec 2015 10:50:41 +0000

django-jinja (1.4.1-2) unstable; urgency=medium

  * Update Vcs-* control headers.
  * Remove unused Build-Depends.
  * Fix mistake in description of python3-django-jinja.
  * This package doesn't include a test suite. There is one in the the
    upstream git repository but it isn't included in the release tarball. The
    Python 3.5 unittest modules tries to load the module even if there is not
    test suite. This fails because the module isn't designed to be run outside
    django. Disable dh_auto_test. (Closes: #801932, #802066)

 -- Edward Betts <edward@4angle.com>  Fri, 16 Oct 2015 18:07:50 +0100

django-jinja (1.4.1-1) unstable; urgency=low

  * Initial release. (Closes: #783793)

 -- Edward Betts <edward@4angle.com>  Wed, 19 Aug 2015 14:47:20 +0200
